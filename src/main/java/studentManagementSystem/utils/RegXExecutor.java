package studentManagementSystem.utils;

import java.util.regex.Pattern;

public class RegXExecutor {
    //EXPRESSIONS FOR VALIDATE
    private static final String utcDateValidation = "(.*)(\\d+)(.*)";

    public static Boolean isValidUtcDate(String text) {
        Pattern pattern = Pattern.compile(utcDateValidation);
        return pattern.matcher(text).matches();
    }
}
