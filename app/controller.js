var myApp = angular.module('app', []);

myApp.controller('appController', function ($filter, $scope, $http, $window, studentService) {
    $scope.isVisible = false;
    $scope.isViewVisible = false;
    $scope.isInsertVisible = false;
    $scope.isDeleteVisible = false;
    $scope.allStudents = [];
    $scope.student = [];
    $scope.names = [];

    $scope.showHideView = function () {

        $scope.spice = 'view';
        $scope.isViewVisible = true;
        $scope.isInsertVisible = false;
        $scope.isDeleteVisible = false;
        $scope.isUpdateVisible = false;

        studentService.getAllStudent().then(function (data) {
            $scope.names = data;
        });
    };
    $scope.showHideInsertStudent = function () {

        $scope.spice = 'insertStudent';
        $scope.isInsertVisible = true;
        $scope.isDeleteVisible = false;
        $scope.isViewVisible = false;
        $scope.isUpdateVisible = false;
    };

    $scope.showHideDeleteStudent = function () {

        $scope.spice = 'deleteStudent';
        $scope.isDeleteVisible = true;
        $scope.isInsertVisible = false;
        $scope.isViewVisible = false;
        $scope.isUpdateVisible = false;
    };
    $scope.showHideUpdate = function (regNo) {

        studentService.getStudent(regNo).then(function (data) {
            $scope.student = data;
            $scope.student.birthday = $filter('date')($scope.student.birthday, "yyyy-MM-dd");
        });
        $scope.isInsertVisible = false;
        $scope.isViewVisible = false;
        $scope.isDeleteVisible = false;
        $scope.isUpdateVisible = true;

    };
    $scope.updateStudent = function () {
        studentService.updateStudent($scope.student.regNo, $scope.student.studentName,
            $scope.student.birthday, $scope.student.address).then(function (data) {
            window.alert("Student updated");
        });

        $scope.showHideView();

    };
    $scope.insertStudent = function () {

        studentService.addUser($scope.student.regNo, $scope.student.studentName,
            $scope.student.birthday, $scope.student.address).then(function (data) {
            window.alert("Student created.");
        });
        $scope.showHideView();
    };
    $scope.deleteStudent = function () {

        studentService.removeStudent($scope.student.regNo).then(function (data) {
            window.alert("Student created");
        });
        $scope.showHideView();
    };

});


