package studentManagementSystem.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import studentManagementSystem.domainClasses.Student;
import studentManagementSystem.services.StudentService;
import studentManagementSystem.utils.ObjectMerger;

import javax.inject.Inject;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/student")
public class StudentController {
    @Inject
    private StudentService studentService;

    @RequestMapping(method = RequestMethod.POST)
    private ResponseEntity<?> insert(@RequestBody Student student) {
        return ResponseEntity.ok(this.studentService.save(student));
    }

    @RequestMapping(method = RequestMethod.DELETE)
    private ResponseEntity<?> delete(@RequestBody Student student) {
        this.studentService.delete(student);
        return ResponseEntity.ok(new Student());
    }

    @RequestMapping(method = RequestMethod.PUT)
    private ResponseEntity<?> update(@RequestBody Student student) {

        Student studentOld = this.studentService.findFirstById(student.getRegNo());
        student = (Student) ObjectMerger.merge(Student.class, studentOld, student);
        this.studentService.update(student);
        return ResponseEntity.ok(student);
    }

    @RequestMapping(method = RequestMethod.GET)
    private ResponseEntity<?> getAll() {
        List<Student> studentList = this.studentService.findAll();
        return ResponseEntity.ok(studentList);
        //return student;
    }

    @RequestMapping(value = "/find", method = RequestMethod.POST)
    private ResponseEntity<?> getStudent(@RequestBody Student student) {

        Student studentFromDb = this.studentService.findFirstById(student.getRegNo());
        return ResponseEntity.ok(studentFromDb);
        //return student;
    }
}
