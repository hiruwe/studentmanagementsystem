package studentManagementSystem.utils;

import java.lang.reflect.Field;

public class ObjectMerger {
    /**
     * APPENDING NEW OBJECT TO OLDER ONE
     *
     * @param entityClass
     * @param old
     * @param latest
     * @return
     */
    public static synchronized Object merge(Class entityClass, Object old, Object latest) {
        if (old == null && latest != null) {
            return latest;
        } else if (old != null && latest == null) {
            return old;
        }
        Field[] entityFields = entityClass.getDeclaredFields();

        for (int i = 0; i < entityFields.length; i++) {
            entityFields[i].setAccessible(true);
            //String fieldName = entityFields[i].getName();
            Object value = null;
            try {
                value = entityFields[i].get(latest);
                if (value != null) {
                    entityFields[i].set(old, value);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return old;
    }
}
