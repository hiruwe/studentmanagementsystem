package studentManagementSystem.dao.Impl;

import com.mongodb.WriteResult;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import studentManagementSystem.dao.StudentDao;
import studentManagementSystem.domainClasses.Student;

import javax.inject.Inject;
import java.util.List;


@Repository
public class StudentDaoImpl implements StudentDao {
    @Inject
    private MongoTemplate mongoTemplate;

    /**
     * SAVING STUDENT OBJECT
     *
     * @param student
     * @return
     */
    @Override
    public Student save(Student student) {
        Query query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "id"));
        query.limit(1);
        Student maxId = mongoTemplate.findOne(query, Student.class);
        if (maxId == null) {
            student.setId(1);
        } else {
            student.setId(maxId.getId() + 1);
        }
        this.mongoTemplate.save(student, "student");
        return student;
    }

    /**
     * FIND STUDENT BY REGISTRATION NUMBER
     *
     * @param regNo
     * @return
     */
    @Override
    public Student findFirstById(String regNo) {
        Query query = new Query(Criteria.where("regNo").is(regNo));
        Student student = this.mongoTemplate.findOne(query, Student.class);
        return student;
    }

    /**
     * GET ALL STUDENT
     *
     * @return
     */
    @Override
    public List<Student> findAll() {
        List<Student> studentList = this.mongoTemplate.findAll(Student.class);
        return studentList;
    }

    /**
     * UPDATE STUDENT
     *
     * @param student
     */
    @Override
    public void update(Student student) {
        Query query = new Query(Criteria.where("regNo").is(student.getRegNo()));
        Update update = new Update();
        update.set("studentName", student.getStudentName());
        update.set("regNo", student.getRegNo());
        update.set("birthday", student.getBirthday());
        update.set("address", student.getAddress());
        WriteResult result = this.mongoTemplate.updateFirst(query, update, Student.class);

    }

    /**
     * DELETE STUDENT
     *
     * @param student
     */
    @Override
    public void delete(Student student) {
        Query query = new Query(Criteria.where("regNo").is(student.getRegNo()));

        this.mongoTemplate.remove(query, Student.class);
    }
}
