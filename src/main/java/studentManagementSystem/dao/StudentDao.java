package studentManagementSystem.dao;

import org.springframework.stereotype.Repository;
import studentManagementSystem.domainClasses.Student;

import java.util.List;

//extends MongoRepository<Student, Long>
@Repository
public interface StudentDao {
    Student save(Student student);

    Student findFirstById(String regNo);

    List<Student> findAll();

    void update(Student student);

    void delete(Student student);
}
