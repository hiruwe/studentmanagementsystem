package studentManagementSystem.services;

import studentManagementSystem.domainClasses.Student;

import java.util.List;

public interface StudentService {
    Student save(Student student);

    Student findFirstById(String regNo);

    List<Student> findAll();

    void update(Student student);

    void delete(Student student);
}
