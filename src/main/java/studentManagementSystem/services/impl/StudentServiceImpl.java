package studentManagementSystem.services.impl;

import org.springframework.stereotype.Service;
import studentManagementSystem.dao.StudentDao;
import studentManagementSystem.domainClasses.Student;
import studentManagementSystem.services.StudentService;

import javax.inject.Inject;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Inject
    private StudentDao studentDao;

    @Override
    public Student save(Student student) {
        return this.studentDao.save(student);
    }

    @Override
    public Student findFirstById(String regNo) {
        return this.studentDao.findFirstById(regNo);
    }

    @Override
    public List<Student> findAll() {
        return this.studentDao.findAll();
    }

    @Override
    public void update(Student student) {
        this.studentDao.update(student);
    }

    @Override
    public void delete(Student student) {
        this.studentDao.delete(student);
    }
}
