myApp.factory('studentService', function ($window, $http) {
    return {
        addUser: function (regNo, studentName, birthday, address) {

            return $http({
                method: "POST",
                url: "http://localhost:8080/student",
                data: {"regNo": regNo, "studentName": studentName, "birthday": birthday, "address": address},
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function mySuccess(response) {
                return response.data;

            }, function myError(response) {
                $window.alert(response.data);
            });
        },
        removeStudent: function (regNo) {
            return $http({
                method: "DELETE",
                url: "http://localhost:8080/student",
                data: {"regNo": regNo},
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function mySuccess(response) {
                return response.data;

            }, function myError(response) {
                $window.alert(response.data);
            });
        },
        updateStudent: function (regNo, studentName, birthday, address) {
            return $http({
                method: "PUT",
                url: "http://localhost:8080/student",
                data: {"regNo": regNo, "studentName": studentName, "birthday": birthday, "address": address},
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function mySuccess(response) {
                return response.data;

            }, function myError(response) {
                $window.alert(response.data);
            });

        },
        getAllStudent: function () {

            return $http({
                method: "GET",
                url: "http://localhost:8080/student",
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function mySuccess(response) {
                return response.data;

            }, function myError(response) {
                $window.alert(response.data);
            });

        },
        getStudent: function (regNo) {
            return $http({
                method: "POST",
                url: "http://localhost:8080/student/find",
                data: {"regNo": regNo},
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function mySuccess(response) {
                return response.data;

            }, function myError(response) {
                $window.alert("Error " + response.data);
            });
        }
    };
});
